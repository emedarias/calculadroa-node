const fs = require('fs');

function sumar(num1, num2)
    {
        resultado=num1+num2;
        const mensaje = num1+" + "+num2+" = "+resultado;
        guardarLog(mensaje);
    }

function restar(num1, num2)
    {
        resultado=num1-num2;
        const mensaje = num1+" - "+num2+" = "+resultado;
        guardarLog(mensaje);
    }

function multiplicar(num1, num2)
    {
        resultado=num1*num2;
        const mensaje = num1+" x "+num2+" = "+resultado;
        guardarLog(mensaje);
    }

function dividir(num1, num2)
    {
        resultado=num1/num2;
        const mensaje = num1+" / "+num2+" = "+resultado;
        guardarLog(mensaje);
    }

function guardarLog(mensaje)
    {
        fs.writeFileSync("log.txt",mensaje+"\n",{flag:"a"})
    }


module.exports =
    {
        "sumar":sumar,
        "restar":restar,
        "multiplicar":multiplicar,
        "dividir":dividir
    }